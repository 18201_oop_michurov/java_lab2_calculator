package ru.nsu.g.mmichurov.calculator.operation.basic;

import org.junit.jupiter.api.Test;

import java.util.Stack;

import static org.junit.jupiter.api.Assertions.*;

class SwapTest {

    @Test
    void apply() {
        var stack = new Stack<Integer>();
        var swap = new Swap();

        stack.push(1);
        stack.push(2);

        swap.apply(stack);

        assertArrayEquals(new Integer[]{ 2, 1 }, stack.toArray());
    }
}