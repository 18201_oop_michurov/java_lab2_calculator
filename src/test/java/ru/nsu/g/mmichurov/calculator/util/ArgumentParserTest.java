package ru.nsu.g.mmichurov.calculator.util;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.NoSuchElementException;

import ru.nsu.g.mmichurov.calculator.util.ArgumentParser.*;


class ArgumentParserTest {

    @Test
    void parse() {
        StringArgument in = new StringArgument(
                'i',
                "in",
                null
        );
        StringArgument out = new StringArgument(
                'o',
                "out",
                "out.txt"
        );
        IntegerArgument n = new IntegerArgument(
                'n',
                "num-of-words",
                2
        );
        IntegerArgument m = new IntegerArgument(
                'm',
                "min-occurrences",
                2
        );
        Flag help = new Flag('h', "help");

        ArgumentParser parser = new ArgumentParser(in, out, m, n, help);
        String[] args = new String[]{
                "0",
                "-i=in.txt",
                "--min-occurrences=10",
                "--out",
                "buff.dat",
                "-h",
                "arg",
                "-n",
                "999"
        };

        parser.parse(args);

        assertTrue(parser.getOption('h').isFound());
        assertTrue(parser.getOption('i').isFound());
        assertEquals(parser.getOptionWithArgument("in").getArgument(), "in.txt");
        assertTrue(parser.getOption('o').isFound());
        assertEquals(parser.getOptionWithArgument("out").getArgument(), "buff.dat");
        assertTrue(parser.getOption("num-of-words").isFound());
        assertEquals(parser.getOptionWithArgument('n').getArgument(), 999);
        assertTrue(parser.getOption("min-occurrences").isFound());
        assertEquals(parser.getOptionWithArgument('m').getArgument(), 10);

        String[] badArgs1 = new String[]{"-k"};
        String[] badArgs2 = new String[]{"-n="};
        String[] badArgs3 = new String[]{"--out", "-h"};
        String[] badArgs4 = new String[]{"-m", "7.62 high velocity"};
        String[] badArgs5 = new String[]{"-h=arg"};

        assertThrows(NoSuchElementException.class, () -> parser.parse(badArgs1));
        assertThrows(IllegalArgumentException.class, () -> parser.parse(badArgs2));
        assertThrows(IllegalArgumentException.class, () -> parser.parse(badArgs3));
        assertThrows(IllegalArgumentException.class, () -> parser.parse(badArgs4));
        assertThrows(IllegalArgumentException.class, () -> parser.parse(badArgs5));
    }

    @Test
    void getOption() {
        StringArgument in = new StringArgument(
                'i',
                "in",
                null
        );
        StringArgument out = new StringArgument(
                'o',
                "out",
                "out.txt"
        );
        IntegerArgument n = new IntegerArgument(
                'n',
                "num-of-words",
                2
        );
        IntegerArgument m = new IntegerArgument(
                'm',
                "min-occurrences",
                2
        );
        Flag help = new Flag('h', "help");

        ArgumentParser parser = new ArgumentParser(in, out, m, n, help);

        assertFalse(parser.getOption('h').isFound());
        assertFalse(parser.getOption('i').isFound());
        assertFalse(parser.getOption('o').isFound());
        assertFalse(parser.getOption("num-of-words").isFound());
        assertFalse(parser.getOption("min-occurrences").isFound());

        assertNull(parser.getOptionWithArgument("in").getArgument());
        assertEquals(parser.getOptionWithArgument("out").getArgument(), "out.txt");
        assertEquals(parser.getOptionWithArgument('n').getArgument(), 2);
        assertEquals(parser.getOptionWithArgument('m').getArgument(), 2);

        assertThrows(ClassCastException.class, () -> parser.getOptionWithArgument('h'));
        assertThrows(ClassCastException.class, () -> parser.getOptionWithArgument("help"));
    }

    @Test
    void getFreeArguments() {
        ArgumentParser parser = new ArgumentParser(
                new StringArgument('n', "n", null),
                new StringArgument('a', "aaa", "default")
        );

        String[] free = new String[]{"arg1", "-n=3", "--aaa", "arg2", "third-arg"};

        parser.parse(free);

        assertEquals(parser.getFreeArguments(), new LinkedList<>(Arrays.asList("arg1", "third-arg")));

        String[] noFree = new String[]{"-n", "value", "-a=value"};

        parser = new ArgumentParser(
                new StringArgument('n', "n", null),
                new StringArgument('a', "aaa", "default")
        );

        parser.parse(noFree);

        assertTrue(parser.getFreeArguments().isEmpty());
    }
}
