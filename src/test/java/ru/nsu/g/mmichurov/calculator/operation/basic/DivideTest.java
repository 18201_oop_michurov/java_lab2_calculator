package ru.nsu.g.mmichurov.calculator.operation.basic;

import org.junit.jupiter.api.Test;

import java.util.Stack;

import static org.junit.jupiter.api.Assertions.*;

class DivideTest {

    @Test
    void apply() {
        var stack = new Stack<Integer>();
        var divide = new Divide();

        stack.push(7);
        stack.push(2);

        divide.apply(stack);

        assertArrayEquals(new Integer[]{ 3 }, stack.toArray());

        stack.pop();
        stack.push(15);
        stack.push(3);

        divide.apply(stack);

        assertArrayEquals(new Integer[]{ 5 }, stack.toArray());

        stack.push(0);

        assertThrows(ArithmeticException.class, () -> divide.apply(stack));
    }
}