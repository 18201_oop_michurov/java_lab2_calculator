package ru.nsu.g.mmichurov.calculator.operation.basic;

import org.junit.jupiter.api.Test;

import java.util.Stack;

import static org.junit.jupiter.api.Assertions.*;

class MultiplyTest {

    @Test
    void apply() {
        var stack = new Stack<Integer>();
        var multiply = new Multiply();

        stack.push(9);
        stack.push(2);

        multiply.apply(stack);

        assertArrayEquals(new Integer[]{ 18 }, stack.toArray());

        stack.pop();
        stack.push(-6);
        stack.push(2);

        multiply.apply(stack);

        assertArrayEquals(new Integer[]{ -12 }, stack.toArray());

        stack.push(-1);

        multiply.apply(stack);

        assertArrayEquals(new Integer[]{ 12 }, stack.toArray());
    }
}