package ru.nsu.g.mmichurov.calculator.operation.basic;

import org.junit.jupiter.api.Test;

import java.util.Stack;

import static org.junit.jupiter.api.Assertions.*;

class SubtractTest {

    @Test
    void apply() {
        var stack = new Stack<Integer>();
        var subtract = new Subtract();

        stack.push(1);
        stack.push(2);

        subtract.apply(stack);

        assertArrayEquals(new Integer[]{ -1 }, stack.toArray());
    }
}