package ru.nsu.g.mmichurov.calculator.operation.advanced;

import org.junit.jupiter.api.Test;
import ru.nsu.g.mmichurov.calculator.operation.Operation;
import ru.nsu.g.mmichurov.calculator.operation.basic.Add;
import ru.nsu.g.mmichurov.calculator.operation.basic.CompareGreater;
import ru.nsu.g.mmichurov.calculator.operation.basic.Multiply;
import ru.nsu.g.mmichurov.calculator.operation.basic.SquareRoot;
import ru.nsu.g.mmichurov.calculator.operation.util.PutInteger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;

import static org.junit.jupiter.api.Assertions.*;

class OperationSequenceTest {

    @Test
    void apply() {
        var stack = new Stack<Integer>();
        var list = new ArrayList<>(
                Arrays.asList(
                        new Add(),
                        new Add(),
                        new Add(),
                        new SquareRoot(),
                        new PutInteger(2),
                        new Multiply()
                )
        );
        var sum4sqrtMul2 = new OperationSequence(list);

        stack.push(1);
        stack.push(2);
        stack.push(7);
        stack.push(6);

        sum4sqrtMul2.apply(stack);

        assertArrayEquals(new Integer[]{ 8 }, stack.toArray());
    }
}