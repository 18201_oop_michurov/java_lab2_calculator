package ru.nsu.g.mmichurov.calculator.operation.basic;

import org.junit.jupiter.api.Test;

import java.util.Stack;

import static org.junit.jupiter.api.Assertions.*;

class CompareLessTest {

    @Test
    void apply() {
        var stack = new Stack<Integer>();
        var compareLess = new CompareLess();

        stack.push(1);
        stack.push(2);

        compareLess.apply(stack);

        assertArrayEquals(new Integer[]{ 1 }, stack.toArray());

        stack.push(-7);

        compareLess.apply(stack);

        assertArrayEquals(new Integer[]{ 0 }, stack.toArray());
    }
}