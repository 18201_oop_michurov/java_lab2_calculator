package ru.nsu.g.mmichurov.calculator.operation.basic;

import org.junit.jupiter.api.Test;

import java.util.Stack;

import static org.junit.jupiter.api.Assertions.*;

class AddTest {

    @Test
    void apply() {
        var stack = new Stack<Integer>();
        var add = new Add();

        stack.push(1);
        stack.push(2);

        add.apply(stack);

        assertArrayEquals(new Integer[]{ 3 }, stack.toArray());
    }
}