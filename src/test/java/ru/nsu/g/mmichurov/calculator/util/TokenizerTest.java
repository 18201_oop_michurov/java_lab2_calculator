package ru.nsu.g.mmichurov.calculator.util;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;

import static org.junit.jupiter.api.Assertions.*;

class TokenizerTest {

    @Test
    void testTokenDetection() {
        String complicatedInput = "1 2-3+ -4- *a.6\n\tdefine3\t-3define\n [print];-1";
        var inputStream = new ByteArrayInputStream(complicatedInput.getBytes());

        var tokenizer = new Tokenizer(inputStream);

        assertTrue(tokenizer.hasNext());
        assertTrue(tokenizer.hasNextInt());
        assertEquals(1, tokenizer.nextInt());
        assertEquals(2, tokenizer.nextInt());
        assertEquals(-3, tokenizer.nextInt());
        assertFalse(tokenizer.hasNext() && tokenizer.hasNextInt());
        assertEquals("+", tokenizer.next());
        assertEquals(-4, tokenizer.nextInt());
        assertEquals("-", tokenizer.next());
        assertEquals("*", tokenizer.next());
        assertEquals("a", tokenizer.next());
        assertEquals(".", tokenizer.next());
        assertEquals(6, tokenizer.nextInt());
        assertEquals("define3", tokenizer.next());
        assertTrue(tokenizer.hasNext() && tokenizer.hasNextInt());
        assertEquals(-3, tokenizer.nextInt());
        assertEquals("define", tokenizer.next());
        assertEquals("[", tokenizer.next());
        assertEquals("print", tokenizer.next());
        assertEquals("]", tokenizer.next());
        assertEquals(";", tokenizer.next());
        assertEquals(-1, tokenizer.nextInt());
        assertThrows(IllegalStateException.class, tokenizer::next);
        assertFalse(tokenizer.hasNext());
    }

    @Test
    void testTokenPositionDetection() {
        String complicatedInput = "\n\tdefine3\t-3define\nabc\n [print];-1\n\n\n\n\n";
        var inputStream = new ByteArrayInputStream(complicatedInput.getBytes());

        var tokenizer = new Tokenizer(inputStream);

        assertEquals("define3", tokenizer.next());
        assertEquals(-3, tokenizer.nextInt());
        assertTrue(tokenizer.hasNext());

        assertEquals("    define3    -3define", tokenizer.getLastTokenLine());
        assertEquals(2, tokenizer.getLastTokenLineNumber());
        assertEquals(15, tokenizer.getLastTokenPosition());

        assertTrue(tokenizer.hasNext());
        assertEquals("define", tokenizer.next());
        assertTrue(tokenizer.hasNext());

        assertEquals("    define3    -3define", tokenizer.getLastTokenLine());
        assertEquals(2, tokenizer.getLastTokenLineNumber());
        assertEquals(17, tokenizer.getLastTokenPosition());

        tokenizer.skipLine();

        assertEquals("    define3    -3define", tokenizer.getLastTokenLine());
        assertEquals(2, tokenizer.getLastTokenLineNumber());
        assertEquals(17, tokenizer.getLastTokenPosition());

        assertTrue(tokenizer.hasNext());
        assertEquals("[", tokenizer.next());
        assertTrue(tokenizer.hasNext());

        assertEquals(" [print];-1", tokenizer.getLastTokenLine());
        assertEquals(4, tokenizer.getLastTokenLineNumber());
        assertEquals(1, tokenizer.getLastTokenPosition());

        tokenizer.next();
        tokenizer.next();
        tokenizer.next();
        tokenizer.nextInt();

        assertFalse(tokenizer.hasNext());

        tokenizer.skipLine();
        tokenizer.skipLine();
        tokenizer.skipLine();
        tokenizer.skipLine();

        assertEquals(" [print];-1", tokenizer.getLastTokenLine());
        assertEquals(4, tokenizer.getLastTokenLineNumber());
        assertEquals(9, tokenizer.getLastTokenPosition());
    }
}