package ru.nsu.g.mmichurov.calculator.operation.basic;

import org.junit.jupiter.api.Test;

import java.util.Stack;

import static org.junit.jupiter.api.Assertions.*;

class CompareGreaterTest {

    @Test
    void apply() {
        var stack = new Stack<Integer>();
        var compareGreater = new CompareGreater();

        stack.push(1);
        stack.push(2);

        compareGreater.apply(stack);

        assertArrayEquals(new Integer[]{ 0 }, stack.toArray());

        stack.push(-7);

        compareGreater.apply(stack);

        assertArrayEquals(new Integer[]{ 1 }, stack.toArray());
    }
}