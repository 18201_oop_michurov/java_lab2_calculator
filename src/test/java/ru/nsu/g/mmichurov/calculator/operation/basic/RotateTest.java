package ru.nsu.g.mmichurov.calculator.operation.basic;

import org.junit.jupiter.api.Test;

import java.util.Stack;

import static org.junit.jupiter.api.Assertions.*;

class RotateTest {

    @Test
    void apply() {
        var stack = new Stack<Integer>();
        var rotate = new Rotate();

        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);

        rotate.apply(stack);
        assertArrayEquals(new Integer[]{ 1, 3, 4, 2 }, stack.toArray());

        rotate.apply(stack);
        assertArrayEquals(new Integer[]{ 1, 4, 2, 3 }, stack.toArray());

        rotate.apply(stack);
        assertArrayEquals(new Integer[]{ 1, 2, 3, 4 }, stack.toArray());
    }
}