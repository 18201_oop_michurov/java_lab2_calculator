package ru.nsu.g.mmichurov.calculator.operation;

import org.junit.jupiter.api.Test;
import ru.nsu.g.mmichurov.calculator.operation.basic.*;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class PredefinedOperationsResolverTest {

    @Test
    void getPredefinedOperation() {
        PredefinedOperationsResolver resolver = null;
        try {
            resolver = new PredefinedOperationsResolver();
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        }

        var op = resolver.getPredefinedOperation("+");
        assertEquals(Add.class, op.getClass());

        op = resolver.getPredefinedOperation(">");
        assertEquals(CompareGreater.class, op.getClass());

        op = resolver.getPredefinedOperation("<");
        assertEquals(CompareLess.class, op.getClass());

        op = resolver.getPredefinedOperation("/");
        assertEquals(Divide.class, op.getClass());

        op = resolver.getPredefinedOperation("drop");
        assertEquals(Drop.class, op.getClass());

        op = resolver.getPredefinedOperation("dup");
        assertEquals(Duplicate.class, op.getClass());

        op = resolver.getPredefinedOperation("*");
        assertEquals(Multiply.class, op.getClass());

        op = resolver.getPredefinedOperation("print");
        assertEquals(Print.class, op.getClass());

        op = resolver.getPredefinedOperation("rot");
        assertEquals(Rotate.class, op.getClass());

        op = resolver.getPredefinedOperation("sqrt");
        assertEquals(SquareRoot.class, op.getClass());

        op = resolver.getPredefinedOperation("-");
        assertEquals(Subtract.class, op.getClass());

        op = resolver.getPredefinedOperation("swap");
        assertEquals(Swap.class, op.getClass());

        final PredefinedOperationsResolver finalResolver = resolver;
        assertThrows(
                UnsupportedOperationException.class,
                () -> finalResolver.getPredefinedOperation("not-a-predefined-operation")
        );
    }

    @Test
    void isPredefinedOperation() {
        PredefinedOperationsResolver resolver = null;
        try {
            resolver = new PredefinedOperationsResolver();
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        }

        assertTrue(resolver.isPredefinedOperation("+"));
        assertTrue(resolver.isPredefinedOperation("*"));
        assertTrue(resolver.isPredefinedOperation("sqrt"));
        assertTrue(resolver.isPredefinedOperation("print"));

        assertFalse(resolver.isPredefinedOperation("define"));
        assertFalse(resolver.isPredefinedOperation("["));
        assertFalse(resolver.isPredefinedOperation("]"));
        assertFalse(resolver.isPredefinedOperation(";"));
        assertFalse(resolver.isPredefinedOperation("not-an-operation"));
    }
}