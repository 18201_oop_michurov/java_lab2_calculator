package ru.nsu.g.mmichurov.calculator.operation.basic;

import org.junit.jupiter.api.Test;

import java.util.Stack;

import static org.junit.jupiter.api.Assertions.*;

class DropTest {

    @Test
    void apply() {
        var stack = new Stack<Integer>();
        var drop = new Drop();

        stack.push(1);
        stack.push(2);

        drop.apply(stack);

        assertArrayEquals(new Integer[]{ 1 }, stack.toArray());
    }
}