package ru.nsu.g.mmichurov.calculator.operation.basic;

import org.junit.jupiter.api.Test;

import java.util.Stack;

import static org.junit.jupiter.api.Assertions.*;

class DuplicateTest {

    @Test
    void apply() {
        var stack = new Stack<Integer>();
        var duplicate = new Duplicate();

        stack.push(1);
        stack.push(2);

        duplicate.apply(stack);

        assertArrayEquals(new Integer[]{ 1, 2, 2 }, stack.toArray());
    }
}