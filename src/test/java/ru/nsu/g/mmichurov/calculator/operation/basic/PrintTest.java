package ru.nsu.g.mmichurov.calculator.operation.basic;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Stack;

import static org.junit.jupiter.api.Assertions.*;

class PrintTest {

    @Test
    void apply() {
        var stack = new Stack<Integer>();
        var outputStream = new ByteArrayOutputStream();
        Print.setOutputStream(new PrintStream(outputStream));
        var print = new Print();

        stack.push(1);
        print.apply(stack);

        stack.push(2);
        print.apply(stack);

        assertEquals("1\n2\n", outputStream.toString());

        Print.setOutputStream(System.out);
    }
}