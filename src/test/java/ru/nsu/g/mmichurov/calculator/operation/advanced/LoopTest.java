package ru.nsu.g.mmichurov.calculator.operation.advanced;

import org.junit.jupiter.api.Test;
import ru.nsu.g.mmichurov.calculator.operation.basic.*;
import ru.nsu.g.mmichurov.calculator.operation.util.PutInteger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;

import static org.junit.jupiter.api.Assertions.*;

class LoopTest {

    @Test
    void apply() {
        var stack = new Stack<Integer>();
        //5
        //1 swap dup [ dup rot * swap 1 - dup ] drop
        var list = new ArrayList<>(
                Arrays.asList(
                        new Duplicate(),
                        new Rotate(),
                        new Multiply(),
                        new Swap(),
                        new PutInteger(1),
                        new Subtract(),
                        new Duplicate()
                )
        );
        var factLoop = new Loop(new OperationSequence(list));
        new PutInteger(5).apply(stack);
        new PutInteger(1).apply(stack);
        new Swap().apply(stack);
        new Duplicate().apply(stack);
        factLoop.apply(stack);
        new Drop().apply(stack);

        assertArrayEquals(new Integer[]{ 120 }, stack.toArray());
    }
}