package ru.nsu.g.mmichurov.calculator.operation.basic;

import org.junit.jupiter.api.Test;

import java.util.Stack;

import static org.junit.jupiter.api.Assertions.*;

class SquareRootTest {

    @Test
    void apply() {
        var stack = new Stack<Integer>();
        var squareRoot = new SquareRoot();

        stack.push(10);
        stack.push(9);

        squareRoot.apply(stack);
        assertArrayEquals(new Integer[]{ 10, 3 }, stack.toArray());

        stack.pop();

        squareRoot.apply(stack);
        assertArrayEquals(new Integer[]{ 3 }, stack.toArray());

        stack.pop();

        stack.push(-1);
        assertThrows(ArithmeticException.class, () -> squareRoot.apply(stack));
    }
}