package ru.nsu.g.mmichurov.calculator.interpreter;

import org.junit.jupiter.api.Test;
import ru.nsu.g.mmichurov.calculator.interpreter.exceptions.InitializationError;
import ru.nsu.g.mmichurov.calculator.interpreter.exceptions.LanguageParsingError;
import ru.nsu.g.mmichurov.calculator.interpreter.exceptions.OperationExecutionError;
import ru.nsu.g.mmichurov.calculator.operation.basic.Print;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class InterpreterTest {

    static class IOBuffer {
        private final ByteArrayInputStream inputStream;
        private final ByteArrayOutputStream outputStream;
        private final PrintStream printStream;

        public IOBuffer(String input) {
            this.inputStream = new ByteArrayInputStream(input.getBytes());
            this.outputStream = new ByteArrayOutputStream();
            this.printStream = new PrintStream(this.outputStream);
        }

        public PrintStream getPrintStream() {
            return printStream;
        }

        public InputStream getInputStream() {
            return this.inputStream;
        }

        public String getResultString() {
            return this.outputStream.toString();
        }
    }

    @Test
    void testPrint() {
        var ioBuffer = new IOBuffer(
                "2 3 + print\n" +
                        "# Вывод: 5"
        );
        Interpreter interpreter = null;

        try {
            interpreter = new Interpreter();
        } catch (InitializationError initializationError) {
            initializationError.printStackTrace();
            fail("Interpreter init error");
        }

        Print.setOutputStream(ioBuffer.getPrintStream());

        try {
            interpreter.execute(ioBuffer.getInputStream(), "test1");
        } catch (OperationExecutionError | LanguageParsingError operationExecutionError) {
            operationExecutionError.printStackTrace();
            fail("Execution error");
        }

        Print.setOutputStream(System.out);

        assertEquals("5\n", ioBuffer.getResultString());
    }

    @Test
    void testFactorial() {
        var ioBuffer = new IOBuffer(
                "define factorial 1 swap dup [ dup rot * swap 1 - dup ] drop ;\n" +
                        "5 factorial print\n" +
                        "# Вывод: 120"
        );
        Interpreter interpreter = null;

        try {
            interpreter = new Interpreter();
        } catch (InitializationError initializationError) {
            initializationError.printStackTrace();
            fail("Interpreter init error");
        }

        Print.setOutputStream(ioBuffer.getPrintStream());

        try {
            interpreter.execute(ioBuffer.getInputStream(), "test1");
        } catch (OperationExecutionError | LanguageParsingError operationExecutionError) {
            operationExecutionError.printStackTrace();
            fail("Execution error");
        }

        Print.setOutputStream(System.out);

        assertEquals("120\n", ioBuffer.getResultString());
    }

    @Test
    void testConditionalPrint() {
        var ioBuffer = new IOBuffer(
                "# Распечатать 1, если 0 < 1\n" +
                        "0 1 < [ 1 print 0 ]\n" +
                        "# Вывод: 1");
        Interpreter interpreter = null;

        try {
            interpreter = new Interpreter();
        } catch (InitializationError initializationError) {
            initializationError.printStackTrace();
            fail("Interpreter init error");
        }

        Print.setOutputStream(ioBuffer.getPrintStream());

        try {
            interpreter.execute(ioBuffer.getInputStream(), "test1");
        } catch (OperationExecutionError | LanguageParsingError operationExecutionError) {
            operationExecutionError.printStackTrace();
            fail("Execution error");
        }

        Print.setOutputStream(System.out);

        assertEquals("1\n", ioBuffer.getResultString());
    }

    @Test
    void testMin() {
        var ioBuffer = new IOBuffer(
                "define min\n" +
                        "  dup rot dup rot 1 rot rot  <\n" +
                        "  [ drop swap drop 0 0 ] \n" +
                        "  [ drop 0 ] ;\n" +
                        "\n" +
                        "5 7 min print\n" +
                        "# Вывод: 5");
        Interpreter interpreter = null;

        try {
            interpreter = new Interpreter();
        } catch (InitializationError initializationError) {
            initializationError.printStackTrace();
            fail("Interpreter init error");
        }

        Print.setOutputStream(ioBuffer.getPrintStream());

        try {
            interpreter.execute(ioBuffer.getInputStream(), "test1");
        } catch (OperationExecutionError | LanguageParsingError operationExecutionError) {
            operationExecutionError.printStackTrace();
            fail("Execution error");
        }

        Print.setOutputStream(System.out);

        assertEquals("5\n", ioBuffer.getResultString());
    }

    @Test
    void testMin2() {
        var ioBuffer = new IOBuffer(
                "define min\n" +
                        "  dup rot dup rot 1 rot rot  <\n" +
                        "  [ drop swap drop 0 0 ] \n" +
                        "  [ drop 0 ] ;\n" +
                        "\n" +
                        "7 5 min print\n" +
                        "# Вывод: 5");
        Interpreter interpreter = null;

        try {
            interpreter = new Interpreter();
        } catch (InitializationError initializationError) {
            initializationError.printStackTrace();
            fail("Interpreter init error");
        }

        Print.setOutputStream(ioBuffer.getPrintStream());

        try {
            interpreter.execute(ioBuffer.getInputStream(), "test1");
        } catch (OperationExecutionError | LanguageParsingError operationExecutionError) {
            operationExecutionError.printStackTrace();
            fail("Execution error");
        }

        Print.setOutputStream(System.out);

        assertEquals("5\n", ioBuffer.getResultString());
    }

    @Test
    void loopPrint() {
        var ioBuffer = new IOBuffer(
                "5\n" +
                        "dup [ print 1 - dup ]");
        Interpreter interpreter = null;

        try {
            interpreter = new Interpreter();
        } catch (InitializationError initializationError) {
            initializationError.printStackTrace();
            fail("Interpreter init error");
        }

        Print.setOutputStream(ioBuffer.getPrintStream());

        try {
            interpreter.execute(ioBuffer.getInputStream(), "test1");
        } catch (OperationExecutionError | LanguageParsingError operationExecutionError) {
            operationExecutionError.printStackTrace();
            fail("Execution error");
        }

        Print.setOutputStream(System.out);

        assertEquals("5\n4\n3\n2\n1\n", ioBuffer.getResultString());
    }

    @Test
    void nestedLoopPrint() {
        var ioBuffer = new IOBuffer(
                "5\n" +
                        "dup [ dup dup [ print 1 - dup ] drop 1 - dup ]");
        Interpreter interpreter = null;

        try {
            interpreter = new Interpreter();
        } catch (InitializationError initializationError) {
            initializationError.printStackTrace();
            fail("Interpreter init error");
        }

        Print.setOutputStream(ioBuffer.getPrintStream());

        try {
            interpreter.execute(ioBuffer.getInputStream(), "test1");
        } catch (OperationExecutionError | LanguageParsingError operationExecutionError) {
            operationExecutionError.printStackTrace();
            fail("Execution error");
        }

        Print.setOutputStream(System.out);

        assertEquals("5\n4\n3\n2\n1\n4\n3\n2\n1\n3\n2\n1\n2\n1\n1\n", ioBuffer.getResultString());
    }
}