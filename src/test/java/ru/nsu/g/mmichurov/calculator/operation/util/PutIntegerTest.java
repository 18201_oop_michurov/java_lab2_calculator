package ru.nsu.g.mmichurov.calculator.operation.util;

import org.junit.jupiter.api.Test;
import ru.nsu.g.mmichurov.calculator.operation.basic.Add;

import java.util.Stack;

import static org.junit.jupiter.api.Assertions.*;

class PutIntegerTest {

    @Test
    void apply() {
        var stack = new Stack<Integer>();
        var putInteger = new PutInteger(1);

        putInteger.apply(stack);
        assertArrayEquals(new Integer[]{ 1 }, stack.toArray());

        putInteger = new PutInteger(2);

        putInteger.apply(stack);
        assertArrayEquals(new Integer[]{ 1, 2 }, stack.toArray());
    }
}