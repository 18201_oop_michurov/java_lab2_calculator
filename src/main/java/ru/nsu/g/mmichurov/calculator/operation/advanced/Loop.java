package ru.nsu.g.mmichurov.calculator.operation.advanced;

import ru.nsu.g.mmichurov.calculator.operation.Operation;

import java.util.EmptyStackException;
import java.util.Stack;

public class Loop implements Operation {
    private final OperationSequence operationSequence;

    public Loop(OperationSequence operationSequence) {
        this.operationSequence = operationSequence;
    }

    @Override
    public void apply(Stack<Integer> stack) throws EmptyStackException {
        while (stack.pop() != 0) {
            this.operationSequence.apply(stack);
        }
    }
}
