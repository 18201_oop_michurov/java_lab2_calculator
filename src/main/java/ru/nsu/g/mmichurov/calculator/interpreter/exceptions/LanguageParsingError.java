package ru.nsu.g.mmichurov.calculator.interpreter.exceptions;

public class LanguageParsingError extends Exception {
    private final String file;
    private final String line;
    private final int lineNumber;
    private final int tokenIndex;

    public LanguageParsingError(
            String file,
            String line,
            int lineNumber,
            int tokenIndex,
            String message) {
        super(message);
        this.file = file;
        this.line = line;
        this.lineNumber = lineNumber;
        this.tokenIndex = tokenIndex;
    }

    public String getDetailedMessage() {
        return String.format(
                "File \"%s\", line %d\n%s\n%s^\n%s: %s",
                this.file,
                this.lineNumber,
                this.line,
                " ".repeat(this.tokenIndex),
                this.getClass().getSimpleName(),
                this.getMessage());
    }
}
