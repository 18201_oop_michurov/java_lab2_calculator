package ru.nsu.g.mmichurov.calculator.interpreter.exceptions;

public class NameError extends LanguageParsingError {

    public NameError(
            String file,
            String line,
            int lineNumber,
            int tokenIndex,
            String message) {
        super(file, line, lineNumber, tokenIndex, message);
    }
}
