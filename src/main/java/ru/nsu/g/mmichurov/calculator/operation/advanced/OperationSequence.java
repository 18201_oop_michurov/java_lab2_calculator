package ru.nsu.g.mmichurov.calculator.operation.advanced;

import ru.nsu.g.mmichurov.calculator.operation.Operation;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.Stack;

public class OperationSequence implements Operation {
    private final ArrayList<Operation> operations;

    public OperationSequence(ArrayList<Operation> operations) {
        this.operations = operations;
    }

    @Override
    public void apply(Stack<Integer> stack) throws EmptyStackException {
        for (var operation : this.operations) {
            operation.apply(stack);
        }
    }
}
