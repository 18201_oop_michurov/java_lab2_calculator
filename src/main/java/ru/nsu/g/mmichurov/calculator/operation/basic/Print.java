package ru.nsu.g.mmichurov.calculator.operation.basic;

import ru.nsu.g.mmichurov.calculator.operation.Operation;

import java.io.PrintStream;
import java.util.EmptyStackException;
import java.util.Stack;

public class Print implements Operation {
    private static PrintStream outputStream = System.out;
    private final PrintStream myStream;

    public Print() {
        this.myStream = outputStream;
    }

    @Override
    public void apply(Stack<Integer> stack) throws EmptyStackException {
        myStream.println(stack.peek());
    }

    public static void setOutputStream(PrintStream newStream) {
        outputStream = newStream;
    }
}
