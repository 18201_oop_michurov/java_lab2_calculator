package ru.nsu.g.mmichurov.calculator.interpreter;

import ru.nsu.g.mmichurov.calculator.interpreter.exceptions.*;
import ru.nsu.g.mmichurov.calculator.operation.*;
import ru.nsu.g.mmichurov.calculator.operation.advanced.*;
import ru.nsu.g.mmichurov.calculator.operation.util.PutInteger;
import ru.nsu.g.mmichurov.calculator.util.Tokenizer;

import java.io.*;
import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.Stack;

public final class Interpreter {
    private final HashMap<String, OperationSequence> definedProcedures = new HashMap<>();
    private final PredefinedOperationsResolver operationResolver;
    private final Stack<Integer> operandStack = new Stack<>();

    private Tokenizer tokenizer;
    private String currentInputFileName;

    public Interpreter()
            throws InitializationError {
        try {
            this.operationResolver = new PredefinedOperationsResolver();
        } catch (IOException e) {
            throw new InitializationError(
                    "Failed to initialize interpreter", e
            );
        }
    }

    public void execute(String inputFileName)
            throws IOException, OperationExecutionError, LanguageParsingError {
        var inputStream = new FileInputStream(new File(inputFileName));

        this.execute(inputStream, inputFileName);

        inputStream.close();
    }

    public void execute()
            throws OperationExecutionError, LanguageParsingError {
        this.execute(System.in, "<System.in>");
    }

    void execute(InputStream inputStream, String inputFileName)
            throws OperationExecutionError, LanguageParsingError {
        this.tokenizer = new Tokenizer(inputStream);
        this.currentInputFileName = inputFileName;

        this.run();
    }

    private class LanguageParser {

        Loop parseLoop()
                throws SyntaxError, NameError {
            var currentOperationList = new ArrayList<Operation>();

            boolean loopBodyEnded = false;

            outer:
            while (Interpreter.this.tokenizer.hasNext()) {
                if (Interpreter.this.tokenizer.hasNextInt()) {
                    currentOperationList.add(new PutInteger(Interpreter.this.tokenizer.nextInt()));
                } else {
                    var token = Interpreter.this.tokenizer.next();

                    switch (token) {
                        case "]":
                            loopBodyEnded = true;
                            break outer;
                        case "#":
                            Interpreter.this.tokenizer.skipLine();
                            break;
                        case "define":
                            Interpreter.this.throwSyntaxError("Function definitions inside loops are not allowed");
                        case "[":
                            currentOperationList.add(this.parseLoop());
                            break;
                        case ";":
                            Interpreter.this.throwSyntaxError(String.format("Stray \"%s\"", token));
                        default:
                            currentOperationList.add(Interpreter.this.getDefinedOperation(token));
                            break;
                    }
                }
            }

            if (!loopBodyEnded) {
                Interpreter.this.throwSyntaxError("Loop is missing a closing \"]\"");
            }

            return new Loop(new OperationSequence(currentOperationList));
        }

        void parseNewProcedure()
                throws SyntaxError, NameError {
            var currentOperationList = new ArrayList<Operation>();

            var procedureName = this.getProcedureName();

            boolean procedureBodyEnded = false;

            outer:
            while (Interpreter.this.tokenizer.hasNext()) {
                if (Interpreter.this.tokenizer.hasNextInt()) {
                    currentOperationList.add(new PutInteger(Interpreter.this.tokenizer.nextInt()));
                } else {
                    var token = Interpreter.this.tokenizer.next();

                    switch (token) {
                        case ";":
                            procedureBodyEnded = true;
                            break outer;
                        case "#":
                            Interpreter.this.tokenizer.skipLine();
                            break;
                        case "define":
                            Interpreter.this.throwSyntaxError("Nested function definitions are not allowed");
                        case "[":
                            currentOperationList.add(this.parseLoop());
                            break;
                        default:
                            currentOperationList.add(Interpreter.this.getDefinedOperation(token));
                            break;
                    }
                }
            }

            if (!procedureBodyEnded) {
                Interpreter.this.throwSyntaxError("Function definition is missing a closing \";\"");
            }

            Interpreter.this.definedProcedures.put(procedureName, new OperationSequence(currentOperationList));
        }

        private String getProcedureName()
                throws SyntaxError, NameError {
            if (Interpreter.this.tokenizer.hasNextInt()) {
                Interpreter.this.tokenizer.nextInt();
                Interpreter.this.throwSyntaxError("Invalid procedure name");
            }

            if (!Interpreter.this.tokenizer.hasNext()) {
                Interpreter.this.throwSyntaxError("No procedure name provided");
            }
            var procedureName = Interpreter.this.tokenizer.next();

            if (procedureName.equals(";")
                    || procedureName.equals("define")
                    || procedureName.equals("[")
                    || procedureName.equals("]")) {
                Interpreter.this.throwSyntaxError("Invalid function name");
            } else if (Interpreter.this.operationResolver.isPredefinedOperation(procedureName)) {
                Interpreter.this.throwNameError(
                        String.format("Redefinition of predefined operation \"%s\"", procedureName)
                );
            } else if (Interpreter.this.definedProcedures.containsKey(procedureName)) {
                Interpreter.this.throwNameError(String.format("Redefinition of procedure \"%s\"", procedureName));
            }

            return procedureName;
        }
    }

    private void run()
            throws OperationExecutionError, LanguageParsingError {
        var parser = new LanguageParser();

        outer:
        while (this.tokenizer.hasNext()) {
            if (this.tokenizer.hasNextInt()) {
                this.operandStack.add(this.tokenizer.nextInt());
            } else {
                var token = this.tokenizer.next();

                Operation currentOperation;

                switch (token) {
                    case "define":
                        parser.parseNewProcedure();
                        continue outer;
                    case "#":
                        this.tokenizer.skipLine();
                        continue outer;
                    case "]":
                    case ";":
                        this.throwSyntaxError(String.format("Stray \"%s\"", token));
                    case "[":
                        currentOperation = parser.parseLoop();
                        break;
                    default:
                        currentOperation = this.getDefinedOperation(token);
                        break;
                }

                this.applyOperation(currentOperation);
            }
        }
    }

    private void applyOperation(Operation operation)
            throws OperationExecutionError {
        try {
            operation.apply(this.operandStack);
        } catch (EmptyStackException | StackOverflowError | ArithmeticException e) {
            String errorMessage;

            if (e instanceof EmptyStackException) {
                errorMessage = "Not enough operands to perform operation";
            } else if (e instanceof StackOverflowError) {
                errorMessage = "Stack overflow";
            } else {
                errorMessage = e.getMessage();
            }

            throw new OperationExecutionError(errorMessage);
        }
    }

    private Operation getDefinedOperation(String token)
            throws NameError {
        Operation operation;

        if (this.definedProcedures.containsKey(token)) {
            operation = this.definedProcedures.get(token);
        } else {
            try {
                operation = this.operationResolver.getPredefinedOperation(token);
            } catch (UnsupportedOperationException e) {
                throw new NameError(
                        this.currentInputFileName,
                        this.tokenizer.getLastTokenLine(),
                        this.tokenizer.getLastTokenLineNumber(),
                        this.tokenizer.getLastTokenPosition(),
                        String.format("\"%s\" does not name a user-defined function nor a predefined operation", token)
                );
            }
        }

        return operation;
    }

    private void throwSyntaxError(String message) throws SyntaxError {
        throw new SyntaxError(
                Interpreter.this.currentInputFileName,
                this.tokenizer.getLastTokenLine(),
                this.tokenizer.getLastTokenLineNumber(),
                this.tokenizer.getLastTokenPosition(),
                message
        );
    }

    private void throwNameError(String message) throws NameError {
        throw new NameError(
                Interpreter.this.currentInputFileName,
                this.tokenizer.getLastTokenLine(),
                this.tokenizer.getLastTokenLineNumber(),
                this.tokenizer.getLastTokenPosition(),
                message
        );
    }
}
