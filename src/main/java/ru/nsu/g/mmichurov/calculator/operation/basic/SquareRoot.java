package ru.nsu.g.mmichurov.calculator.operation.basic;

import ru.nsu.g.mmichurov.calculator.operation.Operation;

import java.util.EmptyStackException;
import java.util.Stack;

public class SquareRoot implements Operation {

    @Override
    public void apply(Stack<Integer> stack) throws EmptyStackException, ArithmeticException {
        int op = stack.pop();

        if (op < 0) {
            throw new ArithmeticException("Cannot extract square root of a negative number");
        }

        stack.push((int) Math.sqrt(op));
    }
}
