package ru.nsu.g.mmichurov.calculator.operation.basic;

import ru.nsu.g.mmichurov.calculator.operation.Operation;

import java.util.EmptyStackException;
import java.util.Stack;

public class Divide implements Operation {

    @Override
    public void apply(Stack<Integer> stack) throws EmptyStackException, ArithmeticException {
        int op2 = stack.pop();
        int op1 = stack.pop();

        stack.push(op1 / op2);
    }
}
