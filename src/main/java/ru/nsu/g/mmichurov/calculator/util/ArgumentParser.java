package ru.nsu.g.mmichurov.calculator.util;

import java.util.*;

public final class ArgumentParser {

    private LinkedList<String> freeArguments;
    private final HashMap<Character, ProgramOption> shortToOption;
    private final HashMap<String, ProgramOption> longToOption;

    public ArgumentParser(ProgramOption... options) {
        this.freeArguments = new LinkedList<>();
        this.shortToOption = new HashMap<>();
        this.longToOption = new HashMap<>();

        for (ProgramOption option : options) {
            this.shortToOption.put(option.getShortOption(), option);
            this.longToOption.put(option.getLongOption(), option);
        }
    }

    public void parse(String[] args) throws IllegalArgumentException, NoSuchElementException {
        for (Iterator<String> it = Arrays.asList(args).iterator(); it.hasNext(); ) {
            String currentArgument = it.next();
            ProgramOption currentOption;

            TYPE type = resolve(currentArgument);

            switch (type) {
                case SHORT_OPTION:
                    try {
                        currentOption = this.getOption(currentArgument.charAt(1));

                        if (currentOption.hasArgument()) {
                            processOptionWithArgument(it, (OptionWithArgument) currentOption);
                        }

                        currentOption.markAsFound();
                    } catch (NoSuchElementException e) {
                        throw new NoSuchElementException(String.format("Unknown option: %s", currentArgument));
                    }
                    break;
                case LONG_OPTION:
                    try {
                        currentOption = this.getOption(currentArgument.substring(2));

                        if (currentOption.hasArgument()) {
                            processOptionWithArgument(it, (OptionWithArgument) currentOption);
                        }

                        currentOption.markAsFound();
                    } catch (NoSuchElementException e) {
                        throw new NoSuchElementException(String.format("Unknown option: %s", currentArgument));
                    }
                    break;
                case ARGUMENT:
                    this.freeArguments.add(currentArgument);
                    break;
                case SHORT_OPTION_AND_ARGUMENT:
                    try {
                        currentOption = this.getOption(currentArgument.charAt(1));
                        int startIndex = currentArgument.indexOf('=');
                        startIndex = startIndex == -1 ? 2 : startIndex + 1;
                        currentArgument = currentArgument.substring(startIndex);

                        processOptionWithArgument(currentOption, currentArgument);

                        currentOption.markAsFound();
                    } catch (NoSuchElementException e) {
                        throw new NoSuchElementException(String.format("Unknown option: %s", currentArgument));
                    }
                    break;
                case LONG_OPTION_AND_ARGUMENT:
                    try {
                        int index = currentArgument.indexOf('=');
                        currentOption = this.getOption(currentArgument.substring(2, index));
                        currentArgument = currentArgument.substring(index + 1);

                        processOptionWithArgument(currentOption, currentArgument);

                        currentOption.markAsFound();
                    } catch (NoSuchElementException e) {
                        throw new NoSuchElementException(String.format("Unknown option: %s", currentArgument));
                    }
                    break;
            }
        }

    }

    public ProgramOption getOption(char shortOption) throws NoSuchElementException {
        ProgramOption option = this.shortToOption.get(shortOption);

        if (option == null) {
            throw new NoSuchElementException(String.format("No such option: -%c", shortOption));
        }

        return option;
    }

    public ProgramOption getOption(String longOption) throws NoSuchElementException {
        ProgramOption option = this.longToOption.get(longOption);

        if (option == null) {
            throw new NoSuchElementException(String.format("No such option: --%s", longOption));
        }

        return option;
    }

    public OptionWithArgument getOptionWithArgument(char shortOption)
            throws NoSuchElementException, ClassCastException {
        ProgramOption option = this.getOption(shortOption);

        if (option instanceof OptionWithArgument) {
            return (OptionWithArgument) option;
        } else {
            throw new ClassCastException(String.format("-%c has no argument", shortOption));
        }
    }

    public OptionWithArgument getOptionWithArgument(String longOption)
            throws NoSuchElementException, ClassCastException {
        ProgramOption option = this.getOption(longOption);

        if (option instanceof OptionWithArgument) {
            return (OptionWithArgument) option;
        } else {
            throw new ClassCastException(String.format("-%s has no argument", longOption));
        }
    }

    public LinkedList<String> getFreeArguments() {
        return this.freeArguments;
    }

    private static void processOptionWithArgument(ProgramOption currentOption, String currentArgument)
            throws IllegalArgumentException {
        if (currentArgument.isEmpty()) {
            throw new IllegalArgumentException(
                    String.format(
                            "Option %s is missing a required argument",
                            currentOption.getShortOption()
                    )
            );
        } else {
            if (currentOption.hasArgument()) {
                try {
                    ((OptionWithArgument) currentOption).parseArgument(currentArgument);
                } catch (IllegalArgumentException e) {
                    throw new IllegalArgumentException(
                            String.format(
                                    "Failed to convert argument %s of " +
                                            "option -%s(--%s) to proper format: %s",
                                    currentArgument,
                                    currentOption.getShortOption(),
                                    currentOption.getLongOption(),
                                    e.getMessage()
                            )
                    );
                }
            } else {
                throw new IllegalArgumentException(
                        String.format(
                                "Option -%s(--%s) takes no arguments but one was provided: %s",
                                currentOption.getShortOption(),
                                currentOption.getLongOption(),
                                currentArgument
                        )
                );
            }
        }
    }

    private static void processOptionWithArgument(Iterator<String> it, OptionWithArgument currentOption)
            throws IllegalArgumentException {
        TYPE type;
        String currentArgument;

        if (it.hasNext()) {
            currentArgument = it.next();
            type = resolve(currentArgument);

            if (type != TYPE.ARGUMENT) {
                throw new IllegalArgumentException(
                        String.format(
                                "Option %s is missing a required argument",
                                currentOption.getShortOption()
                        )
                );
            } else {
                try {
                    currentOption.parseArgument(currentArgument);
                } catch (IllegalArgumentException e) {
                    throw new IllegalArgumentException(
                            String.format(
                                    "Failed to convert argument %s of " +
                                            "option -%s(--%s) to proper format: %s",
                                    currentArgument,
                                    currentOption.getShortOption(),
                                    currentOption.getLongOption(),
                                    e.getMessage()
                            )
                    );
                }
            }
        } else {
            throw new IllegalArgumentException(
                    String.format(
                            "Option %s is missing a required argument",
                            currentOption.getShortOption()
                    )
            );
        }
    }

    private enum TYPE {
        SHORT_OPTION,
        LONG_OPTION,
        ARGUMENT,
        SHORT_OPTION_AND_ARGUMENT,
        LONG_OPTION_AND_ARGUMENT
    }

    private static TYPE resolve(String arg) {
        if (arg.length() > 1) {
            if (arg.charAt(0) == '-') {
                if (arg.charAt(1) == '-' && arg.length() > 2) {
                    return arg.indexOf('=') != -1 ? TYPE.LONG_OPTION_AND_ARGUMENT : TYPE.LONG_OPTION;
                } else if (arg.charAt(1) != '-') {
                    // fix for -n=5 -m5 -k 0 FIXED
                    // fix for -n= FIXED
                    return arg.length() > 2 ? TYPE.SHORT_OPTION_AND_ARGUMENT : TYPE.SHORT_OPTION;
                } else {
                    // fix for -n= FIXED
                    return TYPE.ARGUMENT;
                }
            } else {
                return TYPE.ARGUMENT;
            }
        } else {
            return TYPE.ARGUMENT;
        }
    }

    public static abstract class ProgramOption {
        private final char shortOption;
        private final String longOption;
        private boolean found;

        public ProgramOption(char shortOption, String longOption) {
            this.shortOption = shortOption;
            this.longOption = longOption;
            this.found = false;
        }

        public boolean isFound() {
            return this.found;
        }

        char getShortOption() {
            return this.shortOption;
        }

        String getLongOption() {
            return this.longOption;
        }

        void markAsFound() {
            this.found = true;
        }

        abstract boolean hasArgument();
    }

    public static abstract class OptionWithArgument extends ProgramOption {

        public OptionWithArgument(char shortOption, String longOption) {
            super(shortOption, longOption);
        }

        public abstract Object getArgument();

        abstract void parseArgument(String stringRepresentation);

        @Override
        boolean hasArgument() {
            return true;
        }
    }

    public static class Flag extends ProgramOption {

        public Flag(char shortOption, String longOption) {
            super(shortOption, longOption);
        }

        @Override
        boolean hasArgument() {
            return false;
        }
    }

    public static class StringArgument extends OptionWithArgument {

        private String argument;

        public StringArgument(char shortOption, String longOption, String defaultArgument) {
            super(shortOption, longOption);
            this.argument = defaultArgument;
        }

        @Override
        public String getArgument() {
            return this.argument;
        }

        @Override
        void parseArgument(String stringRepresentation) {
            this.argument = stringRepresentation;
        }
    }

    public static class IntegerArgument extends OptionWithArgument {

        private Integer argument;

        public IntegerArgument(char shortOption, String longOption, Integer defaultArgument) {
            super(shortOption, longOption);
            this.argument = defaultArgument;
        }

        @Override
        public Integer getArgument() {
            return this.argument;
        }

        @Override
        void parseArgument(String stringRepresentation) throws NumberFormatException {
            this.argument = Integer.valueOf(stringRepresentation);
        }
    }
}
