package ru.nsu.g.mmichurov.calculator.operation.basic;

import ru.nsu.g.mmichurov.calculator.operation.Operation;

import java.util.EmptyStackException;
import java.util.Stack;

public class Drop implements Operation {

    @Override
    public void apply(Stack<Integer> stack) throws EmptyStackException {
        stack.pop();
    }
}
