package ru.nsu.g.mmichurov.calculator;
import ru.nsu.g.mmichurov.calculator.interpreter.Interpreter;
import ru.nsu.g.mmichurov.calculator.interpreter.exceptions.*;
import ru.nsu.g.mmichurov.calculator.util.ArgumentParser;
import ru.nsu.g.mmichurov.calculator.util.ArgumentParser.*;

import java.io.*;

public class Application {

    public static void main(String[] args) {
        final var help = new Flag('h', "help");
        final var argumentParser = new ArgumentParser(help);

        argumentParser.parse(args);

        if (help.isFound()) {
            printHelp();
            System.exit(0);
        }

        Interpreter interpreter;

        try {
            interpreter = new Interpreter();
        } catch (InitializationError e) {
            System.err.println(e.getMessage());
            System.err.println(e.getCause().getMessage());
            return;
        }

        try {
            if (argumentParser.getFreeArguments().isEmpty()) {
                interpreter.execute();
            }
            for (var inputFile : argumentParser.getFreeArguments()) {
                interpreter.execute(inputFile);
            }
        } catch (LanguageParsingError | OperationExecutionError | IOException e) {
            if (e instanceof LanguageParsingError) {
                System.err.println(((LanguageParsingError) e).getDetailedMessage());
            } else {
                System.err.println(e.getMessage());
            }
        }
    }

    static void printHelp() {
        System.out.print(
                "Interpreter for stack-based programming language.\n\n" +
                        "USAGE:\n    <executable> [INPUT_FILES...] [KEYS]\n\nKEYS:" +
                        "\n\n    -h, --help                    display help\n\n"
        );
    }
}
