package ru.nsu.g.mmichurov.calculator.util;

import java.io.InputStream;
import java.util.Iterator;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Tokenizer implements Iterator<String> {
    private final Scanner scanner;
    private final Pattern pattern;
    private Matcher matcher;
    private boolean hasNextToken = false;

    private String currentLine = null;
    private int nextLineNumber = 0;
    private int currentLineNumber = 0;
    private int lastTokenPosition = 0;

    private String nextLine = null;

    public Tokenizer(InputStream inputStream) {
        this.scanner = new Scanner(inputStream);
        this.pattern = Pattern.compile("(-?\\d+)|(\\w+)|(\\S+?)");
        this.matcher = this.pattern.matcher("");

        this.tryGetNextToken();
    }

    private void tryGetNextToken() {
        if (!matcher.find()) {
            if (!this.scanner.hasNextLine()) {
                this.hasNextToken = false;
            } else {
                String line;
                do {
                    line = this.scanner.nextLine();
                    this.nextLineNumber += 1;
                } while (line.isEmpty() && this.scanner.hasNextLine());

                this.nextLine = line.replaceAll("\t", "    ");
                this.matcher = pattern.matcher(this.nextLine);

                this.hasNextToken = this.matcher.find();
            }
        } else {
            this.hasNextToken = true;
        }
    }

    @Override
    public boolean hasNext() {
        if (!this.hasNextToken) {
            this.tryGetNextToken();
        }

        return this.hasNextToken;
    }

    public boolean hasNextInt() {
        return this.hasNext() && this.matcher.group(1) != null;
    }

    @Override
    public String next() {
        if (!this.hasNextToken) {
            this.tryGetNextToken();
        }
        this.hasNextToken = false;

        String nextToken = this.matcher.group();

        this.currentLine = this.nextLine;
        this.currentLineNumber = this.nextLineNumber;
        this.lastTokenPosition = this.matcher.start();

        return nextToken;
    }

    public int nextInt() throws NumberFormatException {
        return Integer.parseInt(this.next());
    }

    public void skipLine() {
        this.matcher = this.pattern.matcher("");
        this.hasNextToken = false;
    }

    public String getLastTokenLine() {
        return this.currentLine;
    }

    public int getLastTokenPosition() {
        return this.lastTokenPosition;
    }

    public int getLastTokenLineNumber() {
        return this.currentLineNumber;
    }
}
