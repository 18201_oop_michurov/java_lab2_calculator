package ru.nsu.g.mmichurov.calculator.interpreter.exceptions;

public class InitializationError extends Exception {

    public InitializationError(String message, Throwable cause) {
        super(message, cause);
    }
}
