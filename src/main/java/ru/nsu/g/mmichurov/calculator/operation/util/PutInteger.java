package ru.nsu.g.mmichurov.calculator.operation.util;

import ru.nsu.g.mmichurov.calculator.operation.Operation;

import java.util.Stack;

public class PutInteger implements Operation {
    private final int value;

    public PutInteger(int value) {
        this.value = value;
    }

    @Override
    public void apply(Stack<Integer> stack) {
        stack.push(value);
    }
}
