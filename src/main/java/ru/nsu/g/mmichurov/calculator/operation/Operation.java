package ru.nsu.g.mmichurov.calculator.operation;

import java.util.EmptyStackException;
import java.util.Stack;

public interface Operation {
    void apply(Stack<Integer> stack) throws EmptyStackException, ArithmeticException;
}
