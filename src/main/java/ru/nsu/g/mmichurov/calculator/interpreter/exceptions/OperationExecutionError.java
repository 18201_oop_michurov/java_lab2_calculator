package ru.nsu.g.mmichurov.calculator.interpreter.exceptions;

public class OperationExecutionError extends Exception {

    public OperationExecutionError(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return String.format("%s: %s", this.getClass().getSimpleName(), super.getMessage());
    }
}
