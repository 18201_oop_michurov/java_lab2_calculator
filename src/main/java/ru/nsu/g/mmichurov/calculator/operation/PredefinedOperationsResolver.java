package ru.nsu.g.mmichurov.calculator.operation;

import java.io.FileNotFoundException;
import java.io.IOException;

import java.lang.reflect.InvocationTargetException;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

public class PredefinedOperationsResolver {
    private final String basePackage;
    private final Properties properties;
    private final Set<String> predefinedOperations;

    public PredefinedOperationsResolver() throws IOException {
        this.properties = new Properties();
        var resource = PredefinedOperationsResolver.class.getResourceAsStream(
                "operations.properties");

        if (resource == null) {
            throw new FileNotFoundException("Failed to open properties file \"operations.properties\"");
        }

        this.properties.load(resource);

        basePackage = this.properties.getProperty("basePackage");
        if (basePackage == null) {
            throw new IOException("Properties file missing \"basePackage\" field");
        }

        this.predefinedOperations = this.properties.keySet()
                .stream()
                .map(x -> (String) x)
                .filter(x -> !x.equals("basePackage"))
                .collect(Collectors.toSet());
    }

    public Operation getPredefinedOperation(String token)
            throws UnsupportedOperationException {
        var className = this.properties.getProperty(token);

        if (className == null) {
            throw new UnsupportedOperationException(
                    String.format("Cannot get class name for operation \"%s\"", token)
            );
        }

        try {
            return (Operation) Class.forName(this.basePackage + "." + className).getDeclaredConstructor().newInstance();
        } catch (ClassNotFoundException e) {
            throw new UnsupportedOperationException(
                    String.format(
                            "No such class: \"%s\"",
                            this.basePackage + "." + className
                    )
            );
        } catch (NoSuchMethodException e) {
            throw new UnsupportedOperationException(
                    String.format(
                            "Cannot get declared constructor for class \"%s\"",
                            this.basePackage + "." + className
                    )
            );
        } catch (IllegalAccessException e) {
            throw new UnsupportedOperationException(
                    String.format(
                            "Cannot access constructor for class \"%s\"",
                            this.basePackage + "." + className
                    )
            );
        } catch (InstantiationException e) {
            throw new UnsupportedOperationException(
                    String.format(
                            "Cannot create an instance of class \"%s\"",
                            this.basePackage + "." + className
                    )
            );
        } catch (InvocationTargetException e) {
            throw new UnsupportedOperationException(
                    String.format(
                            "Cannot invoke constructor of class \"%s\"",
                            this.basePackage + "." + className
                    )
            );
        } catch (ClassCastException e) {
            throw new UnsupportedOperationException(
                    String.format(
                            "Class \"%s\" does not implement Operation interface",
                            this.basePackage + "." + className
                    )
            );
        }
    }

    public boolean isPredefinedOperation(String token) {
        return this.predefinedOperations.contains(token);
    }
}
