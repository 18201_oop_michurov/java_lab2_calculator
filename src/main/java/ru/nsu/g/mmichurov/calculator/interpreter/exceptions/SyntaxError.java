package ru.nsu.g.mmichurov.calculator.interpreter.exceptions;

public class SyntaxError extends LanguageParsingError {

    public SyntaxError(
            String file,
            String line,
            int lineNumber,
            int tokenIndex,
            String message) {
        super(file, line, lineNumber, tokenIndex, message);
    }
}
