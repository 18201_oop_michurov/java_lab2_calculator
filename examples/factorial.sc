# This function computes a factorial of a non-negative number
# Number is replaced with it's factorial

define factorial
    1 swap dup
    [ dup rot * swap 1 - dup ]
    drop
;