# This program print factorial for numbers from 1 to 9
# Depends on factorial function which replaces a non-negative number with it's factorial

define main
    0 # Indicates stack bottom
    9
    dup
    [
        dup
        factorial
        swap
        1
        -
        dup
    ]
    drop # Drop 0

    # Print factorials from top to bottom
    dup
    [
        print
        drop
        dup
    ]
    drop
;

main
