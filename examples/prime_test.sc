# source: https://gitlab.com/avoronkov/go-fort-lab/-/raw/master/examples/09.prime.txt

# dup2: a b -> a b a b
define dup2  swap dup rot dup rot swap ;

# not: true -> false; false -> true
define not 1 swap [ drop 0 0 0 ] [ 1 0 ] ;

# modulo: a b -> a%b
define mod  dup2 < not [ dup rot swap - swap dup2 < not ] drop ;

# equal: a b -> a=b
define =  - not ;

# cm: P i -> 0 0  # not prime
#         -> P i+1 1  # continue
define cm  dup2 mod not
  1 swap [ drop drop drop  0 0 0  0 ]
  [ 1 + 1  0 ] ;

# iter: P i -> 1 0 # P < i*i (prime)
#           -> 0 0 # not prime
#           -> P i+1 1 # continue
define iter  dup2 dup * <
  1 swap [ drop drop drop 1 0 0 0 ]
  [ cm 0 ] ;

# prime N -> bool
# Test if given number is prime.
define prime 2 1 [ iter ] ;

2 print prime print drop
3 print prime print drop
4 print prime print drop
5 print prime print drop
6 print prime print drop
7 print prime print drop
8 print prime print drop
9 print prime print drop
10 print prime print drop
13 print prime print drop
16 print prime print drop
24 print prime print drop
25 print prime print drop
29 print prime print drop